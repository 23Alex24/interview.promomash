# Тестовое задание Promomash

.NET Core, Angular application

Tech requirements:

* Front-end: Angular
* Back-end: ASP.NET Core 2.1 Web API
* ORM: Entity Framework Core with migrations and in-memory provider
* No design is required, optionally Bootstrap can be used as a design framework
The result should be presented as working Visual Studio solution / project. F5 to get working.
Beside this, feel free to choose any other libraries, frameworks, patterns, test (fake) data, etc. For example you database with country/province can contain only 2 test countries and 2-3 test provinces for each country. (Country 1, Province 1.1, Province 1.2, is also okay). This data should be seeded on application start.
If you miss some information, do not ask - do it in the way you think it should be done correctly.

Description:
You need to create a simple web page which should contain a registration wizard. The registration wizard contains two steps:

Step 1: (all fields are required)
Login - valid email.
Password - must contain min 1 digit and min 1 letter.
Confirm password - must be the same with the field ”password”.
I agree checkbox - is a required checkbox.
Button next - should validate all fields on the step and show validation errors (under the fields) or go to next step.

Step 2: (all fields are required)
Country - drop down list which contains list of counties.
Province - contains list of provinces for selected country. The list of provinces should be loaded by AJAX if country is changed.
Button save - should validate all fields on the step and show validation errors (under the fields) or save the data from the wizard to the database using AJAX call.

![11.png](https://gitlab.com/23Alex24/interview.promomash/raw/master/images/11.png)