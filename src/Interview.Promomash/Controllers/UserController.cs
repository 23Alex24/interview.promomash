﻿using System;
using System.Threading.Tasks;
using Interview.Promomash.Attributes;
using Interview.Promomash.Business;
using Interview.Promomash.Business.Commands;
using Interview.Promomash.Business.Errors;
using Interview.Promomash.Core.Services;
using Interview.Promomash.Requests.User;
using Interview.Promomash.Responses.Base;
using Interview.Promomash.Responses.User;
using Microsoft.AspNetCore.Mvc;
using Interview.Promomash;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Extensions.DependencyInjection;

namespace Interview.Promomash.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IServiceProvider _iocContainer;

        public UserController(IUserService userService, IServiceProvider iocContainer)
        {
            this._userService = userService;
            this._iocContainer = iocContainer;
        }



        [ValidateRequest]
        [HttpGet("checkemail")]
        public async Task<ActionResult<CheckEmailResponse>> CheckEmail([FromQuery]CheckEmailRequest request)
        {
            var isUsed = await _userService.EmailIsUsed(request.Email);
            var response = new CheckEmailResponse() {AlreadyUsed = isUsed};

            if (isUsed)
            {
                response.ErrorMessage = UserErrors.EmailAlreadyUsed.GetErrorMessage();
                response.ErrorCode = (int) UserErrors.EmailAlreadyUsed;
            }

            return response;
        }
        
        [ValidateRequest]
        [HttpPost("register")]
        public async Task<ActionResult<EmptyResponse>> Register([FromBody] RegisterUserRequest request)
        {
            var command = _iocContainer.GetService<ICommand<RegisterUserCommandArgs, EmptyCommandResult>>();
            var result = await command.Execute(new RegisterUserCommandArgs()
            {
                CityId = request.CityId,
                Password = request.Password,
                Email = request.Email
            });

            return result.ConvertToResponse<EmptyResponse>();
        }
    }
}
