﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Interview.Promomash.Business.Errors;
using Interview.Promomash.Core.Services;
using Interview.Promomash.Responses.Country;
using Microsoft.AspNetCore.Mvc;

namespace Interview.Promomash.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CountriesController : ControllerBase
    {
        private readonly ICountryService _countryService;

        public CountriesController(ICountryService countryService)
        {
            this._countryService = countryService;
        }

        /// <summary>
        /// Returns all countries 
        /// </summary>
        [HttpGet("")]
        public async Task<ActionResult<GetCountriesResponse>> GetCountries()
        {
            var countries = await _countryService.GetCountries();
            return new GetCountriesResponse()
            {
                Countries = countries.ToArray()
            };
        }


        /// <summary>
        /// Returns country cities
        /// </summary>
        [HttpGet("{countryId}/cities")]
        public async Task<ActionResult<GetCititesResponse>> GetCities(int countryId)
        {
            if (countryId < 1)
            {
                return new GetCititesResponse()
                {
                    ErrorCode = (int) CountryErrors.CountryNotFound,
                    ErrorMessage = CountryErrors.CountryNotFound.GetErrorMessage()
                };
            }

            var cities = await _countryService.GetCities(countryId);
            return new GetCititesResponse()
            {
                Cities = cities.ToArray()
            };
        }
    }
}
