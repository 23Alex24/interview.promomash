﻿using Interview.Promomash.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;

namespace Interview.Promomash.AppStart
{
    internal static class DbConfig
    {
        public static void Configure(DbContextOptions<PromomashContext> options, bool isMemory)
        {
            using (var context = new PromomashContext(options))
            {
                if (!isMemory)
                {
                    context.Database.Migrate();
                }
                else
                {
                    context.Database.EnsureCreated();
                }
            }
        }
    }
}
