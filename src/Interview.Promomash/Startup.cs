﻿using Autofac;
using Interview.Promomash.AppStart;
using Interview.Promomash.Infrastructure.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Interview.Promomash
{
    public class Startup
    {
        #region Fields, constructor

        private readonly DbContextOptions<PromomashContext> _dbContextOptions;
        private readonly bool _isMemoryDb = true;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            var optionsBuilder = new DbContextOptionsBuilder<PromomashContext>();
            ConfigureDbOptions(optionsBuilder);
            _dbContextOptions = optionsBuilder.Options;
        }

        #endregion



        public IConfiguration Configuration { get; }



        #region ConfigureServices

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });


            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressConsumesConstraintForFormFileParameters = true;
                options.SuppressInferBindingSourcesForParameters = true;
                //WARNING!! By default web api returns 400 http code and not runs action filter!!!
                //This option turns off this mode
                options.SuppressModelStateInvalidFilter = true;
            });

            services.AddDbContext<PromomashContext>(ConfigureDbOptions);
            DbConfig.Configure(_dbContextOptions, _isMemoryDb);
        }

        // ConfigureContainer is where you can register things directly
        // with Autofac. This runs after ConfigureServices so the things
        // here will override registrations made in ConfigureServices.
        // Don't build the container; that gets done for you. If you
        // need a reference to the container, you need to use the
        // "Without ConfigureContainer" mechanism shown later.
        public void ConfigureContainer(ContainerBuilder builder)
        {
            DependencyConfig.Configure(builder, _dbContextOptions);
        }

        #endregion



        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                // To learn more about options for serving an Angular SPA from ASP.NET Core,
                // see https://go.microsoft.com/fwlink/?linkid=864501

                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });
        }


        #region private methods 

        private void ConfigureDbOptions(DbContextOptionsBuilder builder)
        {
            if (_isMemoryDb)
            {
                builder.UseInMemoryDatabase("mvccore");
            }
            else
            {
                builder.UseSqlServer("Server=23ALEX24-NB\\SQLEXPRESS;Database=Promomash;Trusted_Connection=Yes;");
            }
        }

        #endregion
    }
}
