﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interview.Promomash.Responses.Base;

namespace Interview.Promomash
{
    public static class ResponseExtensions
    {
        public static T AddError<T>(this T response, string message, int errorCode) where T : BaseResponse
        {
            response.ErrorMessage = message;
            response.ErrorCode = errorCode;
            return response;
        }
    }
}
