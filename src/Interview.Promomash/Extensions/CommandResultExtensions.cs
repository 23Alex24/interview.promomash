﻿using Interview.Promomash.Business;
using Interview.Promomash.Responses.Base;

namespace Interview.Promomash
{
    public static class CommandResultExtensions
    {
        public static TResponse ConvertToResponse<TResponse>(this ICommandResult commandResult)
            where TResponse : BaseResponse, new()
        {
            var result = new TResponse
            {
                ErrorCode = commandResult.ErrorCode,
                ErrorMessage = commandResult.IsSuccess ? null : commandResult.ErrorMessage
            };
            return result;
        }
    }
}
