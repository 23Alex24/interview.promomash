﻿using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Interview.Promomash.Business.Errors;
using Interview.Promomash.Responses.Base;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Interview.Promomash.Attributes
{
    /// <summary>
    ///    This filter check the data annotation attributes, validate model.
    ///     If model has some errors returns BaseApiResponse 
    /// </summary>
    public class ValidateRequestAttribute : ActionFilterAttribute
    {
        /// <summary>
        ///  Executing before action running
        /// </summary>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Validate(filterContext);
        }

        public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if (Validate(context))
            {
                await next();
            }            
        }

        /// <summary>
        /// Validating request
        /// </summary>
        private bool Validate(ActionExecutingContext actionContext)
        {
            if (actionContext.ModelState.IsValid)
                return true;

            var actionArguments = actionContext.ActionArguments;

            if (actionArguments.Count == 1)
            {
                var request = actionArguments.First();
                if (request.Value == null)
                {
                    actionContext.ModelState.AddModelError("", "Empty request");
                }
            }

            var response = new EmptyResponse()
            {
                ErrorCode = (int)SystemErrors.BadRequest,
                ErrorMessage = actionContext.ModelState.Values.SelectMany(e => e.Errors).FirstOrDefault()?.ErrorMessage
            };

            actionContext.Result = new JsonResult(response);
            return false;
        }
    }
}
