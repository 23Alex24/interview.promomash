import { Component } from '@angular/core';
import { LoaderService } from './loader.service';

@Component({
  selector: 'loader-main',
  templateUrl: './loader.components.html',
})
export class LoaderComponent {
 public service: LoaderService;

  constructor(loaderService: LoaderService) {
    this.service = loaderService;
  }
}
