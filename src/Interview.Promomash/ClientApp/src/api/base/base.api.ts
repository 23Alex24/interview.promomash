import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiResponse } from './api.response';
import { Observable } from 'rxjs/Observable';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { map, catchError } from 'rxjs/operators';
import { HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';

export abstract  class BaseApi {
  constructor(protected _http: HttpClient, protected _baseUrl: string) {

  }

  protected get<T extends ApiResponse>(url: string, empty: T): Promise<T> {
    var requestUrl = this._baseUrl + url;
    empty.errorCode = 500;
    empty.errorMessage = "Internal server error";

    var result = this._http.get<T>(requestUrl).catch(error => {
      return Observable.of(empty);
    });

    return result.toPromise();  
  }


  protected post<TResponse extends ApiResponse>(url: string, args: any, empty: TResponse): Promise<TResponse> {
    var requestUrl = this._baseUrl + url;
    empty.errorCode = 500;
    empty.errorMessage = "Internal server error";

    var result = this._http.post<TResponse>(requestUrl,args).catch(error => {
      return Observable.of(empty);
    });

    return result.toPromise();
  }
}
