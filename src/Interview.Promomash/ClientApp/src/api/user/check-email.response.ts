import { ApiResponse } from '../base/api.response';

export class CheckEmailResponse extends ApiResponse {
  public alreadyUsed : boolean;
}
