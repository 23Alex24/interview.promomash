import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseApi } from '../base/base.api';
import { GetCountriesResponse } from "./get-countries.response";
import { GetCitiesResponse } from "./get-cities.response";

@Injectable()
export class CountriesApi extends BaseApi {
  private _apiUrl: string;

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    super(http, baseUrl);
    this._apiUrl = "api/countries/";
  }

  public getCountries(): Promise<GetCountriesResponse> {
    return this.get(this._apiUrl, new GetCountriesResponse());
  }

  public getCities(countryId: number): Promise<GetCitiesResponse> {
    return this.get(this._apiUrl + countryId.toString() + "/cities", new GetCitiesResponse());
  }
}
