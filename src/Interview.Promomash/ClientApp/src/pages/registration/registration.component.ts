import { Component } from '@angular/core';
import { RegistrationStep1Model } from "./registration-step1.model";
import { RegistrationStep2Model } from "./registration-step2.model";
import { LoaderService } from "../../components/loader/loader.service";
import { UserApi } from "../../api/user/user.api";
import { CountriesApi } from "../../api/countries/countries.api";

@Component({
  selector: 'registration-block',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent {
  public step1Model: RegistrationStep1Model;
  public step2Model: RegistrationStep2Model;
  public showErrors: boolean;
  public step: number;
  public message: string;
  public messageIsError: boolean;
  
  constructor(
    private _loaderService: LoaderService,
    private _userApi: UserApi,
    private _countriesService: CountriesApi) {

    this.step1Model = new RegistrationStep1Model();
    this.showErrors = false;
    this.step = 1;
  }


  // Go to step 2 click
  public goToStep2() {
    this.setMessage("", true);
    this.showErrors = true;
    this._loaderService.show = true;

    //validate
    if (!this.step1Model.validateEmail().isValid ||
      !this.step1Model.validatePassword().isValid ||
      !this.step1Model.validateConfirmPassword().isValid ||
      !this.step1Model.iAgree) {

      this._loaderService.show = false;
      return;
    }

    //check email using
    this._userApi.checkEmail(this.step1Model.email).then(x => {
      if (!x.errorCode) {
        this.initStep2();
        return;
      }

      this.setMessage(x.errorMessage, true);
    });
  }


  // int step 2
  private initStep2() {
    if (this.step2Model) {
      this.showErrors = false;
      this.step = 2;
      this._loaderService.show = false;
      return;
    }

    this._countriesService.getCountries().then(x => {
      if (!x.errorCode) {
        this.step2Model = new RegistrationStep2Model(x.countries);
        this.showErrors = false;
        this.step = 2;
        this._loaderService.show = false;
        return;
      }

      this.setMessage(x.errorMessage, true);
    });   
  }


  // on country changed (step 2)
  public countryChanged() {
    this.step2Model.resetCities();

    if (this.step2Model.countryId < 1) {
      return;
    }

    this._loaderService.show = true;
    this._countriesService.getCities(this.step2Model.countryId).then(x => {
      if (!x.errorCode) {
        this.step2Model.cities = x.cities;
        this._loaderService.show = false;
        return;
      }

      this.step2Model.countryId = 0;
      this.setMessage(x.errorMessage, true);
    });
  }


  //btn save clicked
  public saveUser() {
    this.showErrors = true;

    if (!this.step2Model.isValid()) {
      return;
    }

    this._loaderService.show = true;
    this._userApi.register(this.step1Model.email,
      this.step1Model.password,
      this.step1Model.confirmPassword,
      this.step2Model.cityId,
      this.step1Model.iAgree).then(x => {

        if (!x.errorCode) {
          this._loaderService.show = false;
          this.step = 3;
          this.setMessage("Congratulations! You successfully registered", false);
          return;
        }

        if (x.errorCode === 101001) {
          this.step2Model.clear();
          this.step = 1;
          this.setMessage(x.errorMessage, true);
        }
    });
  }


  public goToStep1() {
    this.step2Model.clear();
    this.step1Model.clear();
    this.showErrors = false;
    this.setMessage("", false);
    this.step = 1;
  }

  // set message for backend error or success message
  private setMessage(message: string, isError: boolean) {
    this.message = message;
    this.messageIsError = isError;
    this._loaderService.show = false;
  }
}

