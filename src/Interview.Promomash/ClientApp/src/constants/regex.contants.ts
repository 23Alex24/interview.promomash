export class RegexConstants {
  static email: RegExp = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+[.][a-zA-Z0-9-]+$/;

  static password: RegExp = /^\d+[a-zA-Z]+[a-zA-Z0-9]*|[a-zA-Z]+\d+[a-zA-Z0-9]*$/;
}
