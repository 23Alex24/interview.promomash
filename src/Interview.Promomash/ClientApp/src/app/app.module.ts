import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { RegistrationComponent } from '../pages/registration/registration.component';
import { NgxLoadingModule, ngxLoadingAnimationTypes  } from 'ngx-loading';
import {LoaderComponent} from '../components/loader/loader.component';
import { LoaderService } from '../components/loader/loader.service';
import { UserApi } from "../api/user/user.api";
import { CountriesApi } from "../api/countries/countries.api";

@NgModule({
  declarations: [
    AppComponent,
    RegistrationComponent,
    LoaderComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    NgxLoadingModule.forRoot({animationType: ngxLoadingAnimationTypes.rectangleBounce,
      backdropBackgroundColour: 'rgba(128, 128, 128,0.9)', 
      primaryColour: '#ffffff', 
      secondaryColour: '#ffffff', 
      tertiaryColour: '#ffffff'}),
    RouterModule.forRoot([
      { path: '', component: AppComponent, pathMatch: 'full' }
    ])
  ],
  providers: [LoaderService, UserApi, CountriesApi],
  bootstrap: [AppComponent]
})
export class AppModule { }
