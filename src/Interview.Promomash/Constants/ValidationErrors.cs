﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Interview.Promomash.Constants
{
    public static class ValidationErrors
    {
        public const string MIN_LENGTH = "{0} has a minimum length of {1} characters";

        public const string MAX_LENGTH = "{0} has a maximum length of {1} characters";

        public const string INVALID_EMAIL = "Enter a valid email";

        public const string INVALID_PASSWORD = "Password must contains min 1 digit and min 1 letter.";

        public const string CITY_NOT_FOUND = "City not found";

        public const string USER_NOT_AGREE = "You can't register without consent to our rules of work";

        public const string PASSWRODS_NOT_EQUALS = "Password and password confirmation not equals";
    }
}
