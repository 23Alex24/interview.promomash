﻿using System;
using System.Runtime.CompilerServices;
using System.Text;
using Interview.Promomash.Common;
using Interview.Promomash.Common.Utils;
[assembly: InternalsVisibleTo(InternalsVisibleConstants.DEPENDENCY_PROJECT_NAME)]
[assembly: InternalsVisibleTo(InternalsVisibleConstants.Tests.UTILS_TESTS)]

namespace Interview.Promomash.Infrastructure.Utils.Cryprography
{
    /// <summary>
    /// Implementation of ICryptographyUtility
    /// </summary>
    internal class CryptographyUtility : ICryptographyUtility
    {
        private const int _SALT_POW = 3;

        public string ComputeHashCode(HashCodeAlgorithm algorithm, string value)
        {
            var originalBytes = Encoding.UTF8.GetBytes(value);
            var hashBytes = ReverseAndRemove(originalBytes.CalcHashCode(algorithm)).CalcHashCode(algorithm);            
            return  hashBytes.GetString();
        }

        public string ComputeHashCode(HashCodeAlgorithm algorithm, string value, string salt)
        {
            var valueHashCode = ComputeHashCode(algorithm, value);
            var saltBytes = Encoding.UTF8.GetBytes(salt);
            for (int i = 0; i < saltBytes.Length; i++)
            {
                unchecked
                {
                    saltBytes[i] = (byte)(saltBytes[i] * _SALT_POW);
                }                
            }

            var part1 = valueHashCode.Substring(0, valueHashCode.Length / _SALT_POW);
            var part3 = valueHashCode.Substring(valueHashCode.Length / _SALT_POW);
            var resultString = $"{part1}{saltBytes.GetString()}{part3}";
            var hashCode = Encoding.UTF8.GetBytes(resultString).CalcHashCode(algorithm);
            return hashCode.GetString();
        }

        private byte[] ReverseAndRemove(byte[] original, int removeStep = 3)
        {
            var size = original.Length - (original.Length / removeStep);
            var result = new byte[size];

            for (int i = original.Length - 1, j = 0; i >= 0; i--)
            {
                if (i % removeStep == 0)
                    continue;

                result[j] = original[i];
                j++;
            }

            return result;
        }
    }
}
