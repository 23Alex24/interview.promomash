﻿using System;
using System.Security.Cryptography;
using Interview.Promomash.Common.Utils;

namespace Interview.Promomash.Infrastructure.Utils.Cryprography
{
    internal static class CryptographyExtensions
    {
        public static byte[] CalcHashCode(this byte[] bytes, HashCodeAlgorithm algorithm)
        {
            byte[] hash = null;

            switch (algorithm)
            {
                case HashCodeAlgorithm.Md5:
                {
                    using (var provider = MD5.Create())
                    {
                        hash = provider.ComputeHash(bytes);
                    }

                    break;
                }
                case HashCodeAlgorithm.Sha256:
                {
                    using (var provider = SHA256.Create())
                    {
                        hash = provider.ComputeHash(bytes);
                    }

                    break;
                }
                case HashCodeAlgorithm.Sha512:
                {
                    using (var provider = SHA512.Create())
                    {
                        hash = provider.ComputeHash(bytes);
                    }

                    break;
                }
                default:
                {
                    throw new InvalidOperationException("Unknown value of hash code algorithm");
                }
            }

            return hash;
        }


        public static string GetString(this byte[] bytes)
        {
            return BitConverter.ToString(bytes).Replace("-", String.Empty);
        }
    }
}
