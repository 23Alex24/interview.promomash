﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interview.Promomash.Business.Errors
{
    public enum SystemErrors
    {
        Undefined = 0,

        BadRequest = 400,

        InternalServerError = 500
    }
}
