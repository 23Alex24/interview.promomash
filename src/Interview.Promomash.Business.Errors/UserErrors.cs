﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interview.Promomash.Business.Errors
{
    public enum UserErrors
    {
        Undefined = 0,

        EmailAlreadyUsed = 101001
    }
}
