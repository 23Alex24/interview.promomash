﻿using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Interview.Promomash.Business.Errors;
using Interview.Promomash.Common;

[assembly: InternalsVisibleTo(InternalsVisibleConstants.DEPENDENCY_PROJECT_NAME)]

namespace Interview.Promomash.Business.Commands
{
    /// <summary>
    /// Base class for business logic
    /// WARNING!!! command must have a unique combination of args and result (see settings for IoC container)
    /// </summary>
    /// <typeparam name="TArguments">Arguments type for command</typeparam>
    /// <typeparam name="TResult">Result type for command</typeparam>    
    internal abstract class BaseCommand<TArguments, TResult> : ICommand<TArguments, TResult>
            where TArguments : class, ICommandArguments
            where TResult : class, ICommandResult, new()
    {
        /// <summary>
        /// Execute command
        /// </summary>
        public virtual async Task<TResult> Execute(TArguments arguments)
        {
            var type = this.GetType().ToString();

            try
            {                
                var validateResult = await Validate(arguments);

                if (validateResult.IsSuccess)
                {                 
                    var performResult = await InternalExecute(arguments);
                    return performResult;
                }

                return validateResult;
            }
            catch (Exception ex)
            {
                return new TResult()
                {
                    IsSuccess = false,
                    ErrorCode = (int)SystemErrors.InternalServerError,
                    ErrorMessage = SystemErrors.InternalServerError.GetErrorMessage()
                };
            }
        }

        /// <summary>
        /// This method must have contains all working logic (after validation)
        /// </summary>
        protected abstract Task<TResult> InternalExecute(TArguments arguments);

        /// <summary>
        /// This method must have contains all business logic validation (Performs before InternalExecute)
        /// </summary>
        protected abstract Task<TResult> Validate(TArguments arguments);
    }
}
