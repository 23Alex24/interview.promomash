﻿namespace Interview.Promomash.Common
{
    /// <summary>
    /// Constants for InvernalsVisibleTo attributes
    /// </summary>
    public static class InternalsVisibleConstants
    {
        public const string API_RPOJECT_NAME = "Interview.Promomash";

        public const string DEPENDENCY_PROJECT_NAME = "Interview.Promomash.Infrastructure.DependencyResolution";

        public const string ORM_PROJECT_NAME = "Interview.Promomash.Infrastructure.Data";

        public class Tests
        {
            public const string UTILS_TESTS = "Interview.Promomash.UnitTests.Utilities";
        }
    }
}
