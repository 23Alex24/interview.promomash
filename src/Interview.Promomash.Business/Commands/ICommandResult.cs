﻿namespace Interview.Promomash.Business
{
    /// <summary>
    /// Result of working business command
    /// </summary>
    public interface ICommandResult
    {
        bool IsSuccess { get; set; }

        string ErrorMessage { get; set; }

        int ErrorCode { get; set; }
    }
}
