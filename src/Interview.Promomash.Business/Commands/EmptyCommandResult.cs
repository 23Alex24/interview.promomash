﻿namespace Interview.Promomash.Business
{
    /// <summary>
    /// Empty result for business commands that returns void
    /// </summary>
    public class EmptyCommandResult : BaseCommandResult
    {
    }
}
