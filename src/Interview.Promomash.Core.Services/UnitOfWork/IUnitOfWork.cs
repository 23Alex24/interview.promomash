﻿using Interview.Promomash.Core.Entities;
using System;
using System.Data;
using System.Threading.Tasks;

namespace Interview.Promomash.Core.Services
{
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// Returns repository for working with entity
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        Repository<TEntity> GetRepository<TEntity>() where TEntity : class, IEntity;

        /// <summary>
        /// Asynchronously save changes
        /// </summary>
        /// <exception cref="UnitOfWorkException">Throws when save changes failed</exception>
        Task SaveChangesAsync();

        void BeginTransaction();
       
        void BeginTransaction(IsolationLevel isolationLevel);

        void RollBackTransaction();

        void CommitTransaction();
    }
}
