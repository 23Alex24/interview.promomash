﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using Interview.Promomash.Common.Utils;
using Interview.Promomash.Infrastructure.Utils.Cryprography;
using NUnit.Framework;

namespace Interview.Promomash.UnitTests.Utilities
{
    [TestFixture]
    public class CryptographyUtilityTests
    {
        private const string _VALUE1 = "Q123456Q";
        private const string _VALUE2 = "Q123457Q";
        private const string _SALT1 = "a";
        private const string _SALT2 = "b";

        #region ComputeHash

        /// <summary>
        /// Method: ComputeHashCode
        /// Test case: send not exists algorithm type
        /// Expected: Throws exception
        /// </summary>
        [Test]
        public void ComputeHashCode_SendInvalidAlgo_ThrowsException()
        {
            var util = new CryptographyUtility();
            var incorrectAlgorithm = (HashCodeAlgorithm)192342345;
            Assert.Throws<InvalidOperationException>(() => util.ComputeHashCode(incorrectAlgorithm, _VALUE1));
        }


        /// <summary>
        /// Method: ComputeHashCode
        /// Test case: Send valid args
        /// Expected: Valid hash computed
        /// </summary>
        [TestCase(_VALUE1, HashCodeAlgorithm.Md5, "C2C069F3982D492677B4188B3753080D")]
        [TestCase(_VALUE2, HashCodeAlgorithm.Md5, "BF9EC557D098892CAE006E0A6EC74070")]
        [TestCase(_VALUE1, HashCodeAlgorithm.Sha256, "D0D3065EDAD306DA17174861993F7E3E8877DD23F5E24797DED8105B7B6907AD")]
        [TestCase(_VALUE2, HashCodeAlgorithm.Sha256, "C2CE36A9016FD6EB4C742DFA30E7359AB6B0D2D8833EBC825ED4A3AA2F273C92")]
        [TestCase(_VALUE1, HashCodeAlgorithm.Sha512, "48B4DE2BE28F2C6B3EBD84710EE9EF16E6B06DF28D0C98B79813B0BC75972B388C8E46173AD6144AD73FB2E887DAD136E97C0BE522E2EDC06AC8D4E1C0CFE2B4")]
        [TestCase(_VALUE2, HashCodeAlgorithm.Sha512, "51B272E5AB5D8AE8A1D57A35C7D4F388BF804EC29137328CC573D8C17563BE62FEE9D7B5D0884A237298F72B9561E70EFC57003870D0D1291EB53088FFC1C660")]
        public void ComputeHashCode_WithoutSalt_ValidHashCode(string value, HashCodeAlgorithm algo, string expected)
        {
            var util = new CryptographyUtility();
            Assert.AreEqual(expected, util.ComputeHashCode(algo, value));
        }


        /// <summary>
        /// Method: ComputeHash with salt
        /// Test case: send not exists algorithm type
        /// Expected: Throws exception
        /// </summary>
        [Test]
        public void ComputeHashCodeWithSalt_SendInvalidAlgo_ThrowsException()
        {
            var util = new CryptographyUtility();
            var incorrectAlgorithm = (HashCodeAlgorithm)192342345;
            Assert.Throws<InvalidOperationException>(() => util.ComputeHashCode(incorrectAlgorithm, _VALUE1, _VALUE1));
        }

        /// <summary>
        /// Method: ComputeHash with salt
        /// Test case: Compute hash with salt
        /// Expected: Valid hash computed
        /// </summary>
        [TestCase(_VALUE1, HashCodeAlgorithm.Md5,_SALT1, "52FD10FB7BB7922095B270F865368941")]
        [TestCase(_VALUE1, HashCodeAlgorithm.Md5, _SALT2, "C461CCD9CF3355E51F13608F0703F6CB")]
        [TestCase(_VALUE2, HashCodeAlgorithm.Md5, _SALT1, "6AAF4219688B2E47E546B040BEF80380")]
        [TestCase(_VALUE2, HashCodeAlgorithm.Md5, _SALT2, "9238536EEC63407C03A7BE7B7DEBDE0D")]
        [TestCase(_VALUE1, HashCodeAlgorithm.Sha256, _SALT1, "D3CE35DB8DF3AAC1AD2535D7B596F0DAE364AA9CFADB7BE25061914EEC72FEF5")]
        [TestCase(_VALUE1, HashCodeAlgorithm.Sha256, _SALT2, "13F0C7A09E5905608377B42896FBA0CA3E19AC4B212D47DFB0B257346348EA10")]
        [TestCase(_VALUE2, HashCodeAlgorithm.Sha256, _SALT1, "B639EB580E2BA857ACE95DAE6B7915BACE92874567B1D74F3CF2E17E2ACABD64")]
        [TestCase(_VALUE2, HashCodeAlgorithm.Sha256, _SALT2, "7B5121FEA1867A2E2AA3FBCF9D880BE970E928F103DF764B78EBD3A7FBA59719")]
        [TestCase(_VALUE1, HashCodeAlgorithm.Sha512, _SALT1, "AA3812AE6DB818D6FB0902A731743FE1CBE8CA19D8F904989CA97C0FAE8F2EFA0F1972C9BC0875E2E369A79686FB246BDD646667AA147BE86361932AF5CC5596")]
        [TestCase(_VALUE1, HashCodeAlgorithm.Sha512, _SALT2, "5F5B1E09F2DC76A25170B641187685CC17410F35DC1F7EE86918BF8F53FC5EA1C4A668F3F39C08965C2EE4664FCC4AC1CFE85F968CC3DBDAD47AF78565FE8458")]
        [TestCase(_VALUE2, HashCodeAlgorithm.Sha512, _SALT1, "5EE50D5E421A18B2A9201F3D6A4A3E1C7B0DAB95451358DAF5D223D5C2645AD2E620372BB7FD4FB93CB14D51E576D09F117A6D4E366B58D4562E6CE1ACD9BC6C")]
        [TestCase(_VALUE2, HashCodeAlgorithm.Sha512, _SALT2, "55B32E69ED1442DD618AD610B30244E880C3528F76A984B2169997D378C904CD4960D055D294EF51D292674137CC449C32C3A87389DB262076770133577FA3DA")]
        public void ComputeHashCodeWithSalt_WithSalt_ValidHashCode(string value, HashCodeAlgorithm algo,string salt, string expected)
        {
            var util = new CryptographyUtility();
            var a = util.ComputeHashCode(algo, value, salt);
            Assert.AreEqual(expected, util.ComputeHashCode(algo, value, salt));
        }

        #endregion
    }
}