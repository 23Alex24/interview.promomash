﻿using System.Runtime.CompilerServices;
using Interview.Promomash.Common;

[assembly: InternalsVisibleTo(InternalsVisibleConstants.ORM_PROJECT_NAME)]

namespace Interview.Promomash.Core.Entities
{
    /// <summary>
    /// Database entity
    /// </summary>    
    public interface IEntity
    {

    }
}
