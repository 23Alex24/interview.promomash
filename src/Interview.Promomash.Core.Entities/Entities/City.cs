﻿using System.Collections.Generic;

namespace Interview.Promomash.Core.Entities
{
    /// <summary>
    /// City entity
    /// </summary>
    public class City : IEntity
    {
        public const int TITLE_MAX_LENGHT = 300;

        public int Id { get; set; }

        public int CountryId { get; set; }

        public string Title { get; set; }



        /// <summary>
        /// navigation property
        /// </summary>
        internal Country Country { get; set; }

        /// <summary>
        /// navigation property
        /// </summary>
        internal IList<User> Users { get; set; }
    }
}
