# Тестовое задание для Promomash. 

Выполнял в октябре 2018.

#### Документация
* [Тестовое задание](https://gitlab.com/23Alex24/interview.promomash/blob/master/docs/home.md)
* [Скриншоты](https://gitlab.com/23Alex24/interview.promomash/blob/master/docs/screenshots.md)
* [Back-end архитектура](https://gitlab.com/23Alex24/interview.promomash/blob/master/docs/backend.md)
* [Возникшие проблемы при работе над заданием, их решения](https://gitlab.com/23Alex24/interview.promomash/blob/master/docs/problems.md)
* [Бизнес логика, нюансы, валидация](https://gitlab.com/23Alex24/interview.promomash/blob/master/docs/logic.md)

**Использованные технологии и подходы**:

* Луковая архитектура (Onion architecture), Dependency Injection
* C#, .NET core 2.1, .NET standard, ASP.NET core 2.1
* Entity framework core 2.1.3, In-memory provider, MS Sql server, Migrations
* Nunit 3, Autofac (IoC container)
* Html, Angular 5, Angular Cli 1.7, Bootstrap 3, TypeScript 2, Node.js, rxjs
* Паттерны Repository (отличается от обычного использования), UnitOfWork, Command

**Запуск**:

* Visual studio 2017
* Установленный .net core sdk 2.1.4
* Node.js версии 8.1 или выше, Angular/cli 1.7, менеджер пакетов npm
* При запуске выбрать стартуемый проект (.csproj) Interview.Promomash
* Если с первого раза не удалось запустить проект, то можно сделать повторный запуск (при запуске подтягиваются npm пакеты, могут не с первого раза подтянуться). По хорошему front-end проект должен быть отдельным, но в задании было условие запуск из под VS кнопкой F5.